![Ankit Palli](https://gitlab.com/ankitpalli/resume/-/raw/master/assets/ankitpalli.png)

<img alt="Night Coding" src="./assets/Hand%20Wave.gif" width='40' align="left"/><h2>Hey! I'm Ankit</h2>

<!-- ## 👋 &nbsp;Hey there! I'm Aditya -->

### 👨🏻‍💻 &nbsp;About Me

💡 &nbsp;I like to explore new technologies and develop software solutions and quick hacks.\
💬 &nbsp;Feel free to reach out to me for pro bono consulting and volunteering, or just for some interesting discussion.\
✉️ &nbsp;You can shoot me an email at palliankit123@gmail.com! I'll try to respond as soon as I can.\
📄 &nbsp;Please have a look at my [Portfolio](https://freelancerankit.github.io/portfolio) for more details about me. I'm open to feedback and suggestions!

<img alt="Night Coding" src="https://raw.githubusercontent.com/AVS1508/AVS1508/master/assets/Night-Coding.gif" align="right"/>

### 🛠 &nbsp;Tech Stack

![Java](https://img.shields.io/badge/-Java-05122A?style=flat&logo=Java&logoColor=FFA518)&nbsp;
![C](https://img.shields.io/badge/-C-05122A?style=flat&logo=C&logoColor=A8B9CC)&nbsp;
![C++](https://img.shields.io/badge/-C++-05122A?style=flat&logo=C%2B%2B&logoColor=00599C)&nbsp;
![R (Statistics)](https://img.shields.io/badge/-R-05122A?style=flat&logo=R&logoColor=276DC3)\
![React](https://img.shields.io/badge/-React-05122A?style=flat&logo=react)&nbsp;
![Node.js](https://img.shields.io/badge/-Node.js-05122A?style=flat&logo=node.js)&nbsp;
![Bootstrap](https://img.shields.io/badge/-Bootstrap-05122A?style=flat&logo=bootstrap&logoColor=563D7C)\
![HTML](https://img.shields.io/badge/-HTML-05122A?style=flat&logo=HTML5)&nbsp;
![CSS](https://img.shields.io/badge/-CSS-05122A?style=flat&logo=CSS3&logoColor=1572B6)&nbsp;
![Git](https://img.shields.io/badge/-Git-05122A?style=flat&logo=git)&nbsp;
![GitHub](https://img.shields.io/badge/-GitHub-05122A?style=flat&logo=github)&nbsp;
![Markdown](https://img.shields.io/badge/-Markdown-05122A?style=flat&logo=markdown)\
![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-05122A?style=flat&logo=visual-studio-code&logoColor=007ACC)&nbsp;
![RStudio](https://img.shields.io/badge/-RStudio-05122A?style=flat&logo=rstudio)&nbsp;
![Eclipse](https://img.shields.io/badge/-Eclipse-05122A?style=flat&logo=eclipse-ide&logoColor=2C2255)\
![Illustrator](https://img.shields.io/badge/-Illustrator-05122A?style=flat&logo=adobe-illustrator)&nbsp;
![Photoshop](https://img.shields.io/badge/-Photoshop-05122A?style=flat&logo=adobe-photoshop)

### ⚙️ &nbsp;GitHub Analytics

<p align="center">
<a href="https://github.com/FreelancerAnkit">
  <img height="180em" src="https://github-readme-stats-eight-theta.vercel.app/api?username=FreelancerAnkit&show_icons=true&theme=algolia&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats-eight-theta.vercel.app/api/top-langs/?username=FreelancerAnkit&layout=compact&langs_count=8&theme=algolia"/>
</a>
</p>

### 🤝🏻 &nbsp;Connect with Me

<p align="center">
<a href="https://freelancerankit.github.io/portfolio/"><img src="https://img.shields.io/badge/-google.com-3423A6?style=flat&logo=Google-Chrome&logoColor=white"/></a>
<a href="https://www.linkedin.com/in/ankit-3547b8115/"><img src="https://img.shields.io/badge/-Ankit%20Palli%20-0077B5?style=flat&logo=Linkedin&logoColor=white"/></a>
<a href="mailto:palliankit123@gmail.com"><img src="https://img.shields.io/badge/-palliankit123@gmail.com-D14836?style=flat&logo=Gmail&logoColor=white"/></a>
</p>
